/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author OLAF
 */
public class TipoEntidade {

    private String designacao;
    private static final String DESIGN_POR_OMISSAO = "sem designação";

    public TipoEntidade() {
        designacao = DESIGN_POR_OMISSAO;
    }

    public TipoEntidade(String designacao) {
        this.setDesignacao(designacao);
    }

    public final void setDesignacao(String designacao) {
        this.designacao = designacao;
    }

    public String getDesignacao() {
        return designacao;
    }

    // Validação local   
    public boolean valida() {
        // Escrever aqui o código de validação
        return true;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("Tipo de Entidade: " + designacao);
        return sb.toString();
    }

    public String toString2() {
        StringBuffer sb = new StringBuffer();
        sb.append(designacao);
        return sb.toString();
    }
}
