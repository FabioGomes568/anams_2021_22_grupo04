/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.ArrayList;

/**
 *
 * @author OLAF
 */
public class Entidade {

    private String referencia;
    private String designacao;
    private ArrayList<TipoEntidade> listaTipoEnt;
    private int id =0;

    public static final String REFERENCIA_POR_OMISSAO = "";
    public static final String DESIGNACAO_POR_OMISSAO = "";

    public Entidade() {
        referencia = REFERENCIA_POR_OMISSAO;
        designacao = DESIGNACAO_POR_OMISSAO;
        listaTipoEnt = new ArrayList<TipoEntidade>();
    }

    public Entidade(String designacao, ArrayList<TipoEntidade> tipoEnt) {
        id++;
        setReferencia();
        this.designacao = designacao;
        this.listaTipoEnt = tipoEnt;
    }

    public String getReferencia() {
        return referencia;
    }

    public String getDesignacao() {
        return designacao;
    }

    public ArrayList<TipoEntidade> obterListaTipoEnt() {
        return listaTipoEnt;
    }

    public void setReferencia() {
        
        this.referencia = "E" + Integer.toString(id);
    }

    public void setDesignacao(String designacao) {
        this.designacao = designacao;
    }

    public void setTipoEnt(TipoEntidade tipoEnt) {
        this.listaTipoEnt.add(tipoEnt);
    }

    public TipoEntidade oberTipoEntidadeDesignacao(String designacao) {
        TipoEntidade result = null;
        for (TipoEntidade tp : listaTipoEnt) {
            if (tp.getDesignacao().equals(designacao)) {
                result = tp;
            }
        }
        return result;
    }

    public boolean guardaTipoEntidade(TipoEntidade tipoEnt) {
        if (this.validaTipoEntidade(tipoEnt)) {
            return this.listaTipoEnt.add(tipoEnt);
        }
        return false;
    }

    public boolean validaTipoEntidade(TipoEntidade tipoEnt) {
        boolean bRet = false;
        if (tipoEnt.valida()) {
            // Escrever aqui o código de validação

            //
            bRet = true;
        }
        return bRet;
    }

    public boolean valida() {
        // Escrever aqui o códgio de validação
        return true;
    }
    
    

    public String obterDados() {
        StringBuffer sb = new StringBuffer();
        sb.append("Referencia: " + referencia + "\n Designação: " + designacao + "\n" + listaTipoEnt);
        return sb.toString();
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("Entidade:\n" + " Referencia: " + referencia + ",\n" + " Designação: " + designacao + ",\n" + listaTipoEnt + "\n");
        return sb.toString();
    }
}
