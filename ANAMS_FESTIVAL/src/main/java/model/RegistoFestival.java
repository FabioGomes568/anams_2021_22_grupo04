/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import utils.Data;

/**
 *
 * @author joaoferreira
 */
public class RegistoFestival {

    private final List<Festival> lstFestivais;

    public RegistoFestival() {
        this.lstFestivais = new ArrayList<>();
    }

    public List<Festival> getLstFestivais() {
        return lstFestivais;
    }

    private boolean adicionaFestival(Festival fest) {
        return lstFestivais.add(fest);
    }

    public List<Festival> obterListaFestivais() {
        return lstFestivais;
    }

    public Festival novoFestival() {
        return new Festival();
    }

    public boolean validaFestival(Festival fest) {
        boolean bRet = false;
        if (fest.valida()) {
            // Escrever aqui o código de validação

            //
            bRet = true;
        }
        return bRet;
    }

    public boolean registaFestival(Festival fest) {
        if (this.validaFestival(fest)) {
            return adicionaFestival(fest);
        }
        return false;
    }

    public ArrayList<Festival> obterInformacaoFestival(Data data) { //alterei de string para arraylist. confirmar com a professora
        ArrayList<Festival> result = new ArrayList<>();
        for (Festival f : lstFestivais) {
            if (f.getDataInicio().isMaior(data)) {
                result.add(f);
            }
        }
        return result;
    }

    public Festival obterDadosFestivalDesignacao(String designacao) {
        Festival result = null;
        for (Festival f : lstFestivais) {
            if (f.getDesignacao().equals(designacao)) {
                result = f;
            }
        }
        return result;
    }

    public Festival obterFestivalReferencia(String ref) {
        Festival result = null;
        for (Festival f : lstFestivais) {
            if (f.getReferencia().equals(ref)) {
                result = f;
            }
        }
        return result;
    }
    
    public Bilhetica obterBilheticaReferencia(String ref) {
        Festival result = null;
        Bilhetica b = null;
        for (Festival f : lstFestivais) {
            if (f.getReferencia().equals(ref)) {
                b = f.getBilhetica();
            }
        }
        return b;
    }

    public Festival obterDadosFestivalReferencia(String ref) {
        Festival result = null;
        for (Festival f : lstFestivais) {
            if (f.getReferencia().equals(ref)) {
                result = f;
            }
        }
        return result;
    }
    
    public Programa obterProgamaFestival(String ref) {
        Programa result = null;
        for (Festival f : lstFestivais) {
            if (f.getReferencia().equals(ref)) {
                result = f.getProg();
            }
            else {
                System.out.println("Referência incorreta. Introduza novamente.");
            }
        }       
        return result;
    }

    @Override
    public String toString() {
        return "RegistoFestival{" + "lstFestivais=" + lstFestivais + '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RegistoFestival other = (RegistoFestival) obj;
        if (!Objects.equals(this.lstFestivais, other.lstFestivais)) {
            return false;
        }
        return true;
    }

}
