/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author joaoferreira
 */
public class Palco {

    private int id;
    private String designacao;
    private int lotacaoMax;
    private String referencia;

    static int totalId = 0;
    private static final String NOME_POR_OMISSAO = "n/d";
    private static final int LOTACAO_POR_OMISSAO = 0;

    public Palco() {
        this.id = ++totalId;
        this.designacao = NOME_POR_OMISSAO;
        this.lotacaoMax = LOTACAO_POR_OMISSAO;
    }

    public Palco(String designacao, int lotacaoMax) {
        this.id = ++totalId;
        this.designacao = designacao;
        this.lotacaoMax = lotacaoMax;
    }

    public int getId() {
        return id;
    }

    public String getDesignacao() {
        return designacao;
    }

    public int getLotacaoMax() {
        return lotacaoMax;
    }

    public void setId(int i) {
        this.id = i;
    }
    
    public void setReferencia() {
        this.referencia = "P" + Integer.toString(id);
    }

    public void setDesignacao(String designacao) {
        this.designacao = designacao;
    }

    public void setLotacaoMax(int lotacaoMax) {
        this.lotacaoMax = lotacaoMax;
    }
    // não inseri o toString nem o equals por não existir necessidade, para já.
    
    
}
