/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.Objects;

/**
 *
 * @author OLAF
 */
public class TipoBilhete {

    private int id;
    private String designacao;

    private static final int ID_POR_OMISSAO = 0;
    private static final String DESIGNACAO_POR_OMISSAO = "N/D";
    static int totalID = 0;

    public TipoBilhete() {
        this.id = totalID++;
        this.designacao = DESIGNACAO_POR_OMISSAO;
    }

    public TipoBilhete(String desingacao) {
        this.id = totalID++;
        this.designacao = designacao;
    }

    public int getId() {
        return id;
    }

    public String getDesignacao() {
        return designacao;
    }

    public void setDesignacao(String designacao) {
        this.designacao = designacao;
    }

    public static int getTotalID() {
        return totalID;
    }

    @Override
    public String toString() {
        return "TipoBilhete{" + "id=" + id + ", designacao=" + designacao + '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TipoBilhete other = (TipoBilhete) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.designacao, other.designacao)) {
            return false;
        }
        return true;
    }

}
