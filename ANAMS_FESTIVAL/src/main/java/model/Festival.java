/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import utils.Data;

/**
 *
 * @author OLAF
 */
public class Festival {

    private String referencia;
    private String designacao;
    private String edicao;
    private String localizacao;
    private Data dataInicio;
    private Data dataFim;
    private Recinto recinto;
    private List<Entidade> lstEnt;
    private List<Convite> lstConvites;
    private List<AssociacaoEntidade> lstAssEnt;
    private Programa prog;
    private List<Atuacao> lstAtuacoes;
    private Bilhetica bilhetica;
    private ArrayList<TipoBilhete> lstTiposBilhete;
    private int id = 0;

    private static final String DADOS_POR_OMISSAO = "N/A";
    private static final int QUANTIDADE_POR_OMISSAO = 0;

    public Festival() {
        this.referencia = DADOS_POR_OMISSAO;
        this.designacao = DADOS_POR_OMISSAO;
        this.edicao = DADOS_POR_OMISSAO;
        this.localizacao = DADOS_POR_OMISSAO;
        this.dataInicio = new Data();
        this.dataFim = new Data();
        this.recinto = recinto;
        this.lstEnt = new ArrayList<>();
        this.lstConvites = new ArrayList<>();
        this.lstAssEnt = new ArrayList<>();
        this.lstEnt = new ArrayList<>();
        this.prog = new Programa();
        this.lstAtuacoes = new ArrayList<Atuacao>();
        this.bilhetica = new Bilhetica();
        this.lstTiposBilhete = new ArrayList<>();
    }

    public Festival(String referencia, String designacao, String edicao, String localizacao, Data data, Recinto recinto, int qtdPalcos, int lotacaoMaxPalcos, Bilhetica bilhetica) {
        this.referencia = referencia;
        this.designacao = designacao;
        this.edicao = edicao;
        this.localizacao = localizacao;
        this.dataInicio = new Data(dataInicio);
        this.dataFim = new Data(dataFim);
        this.recinto = recinto;
        this.lstEnt = new ArrayList<>();
        this.lstConvites = new ArrayList<>();
        this.lstAssEnt = new ArrayList<>();
        this.prog = new Programa();
        this.lstAtuacoes = new ArrayList<>();
        this.bilhetica = bilhetica;
        this.lstTiposBilhete = new ArrayList<>();
    }

    public String getReferencia() {
        return referencia;
    }

    public String getDesignacao() {
        return designacao;
    }

    public String getEdicao() {
        return edicao;
    }

    public String getLocalizacao() {
        return localizacao;
    }

    public Data getDataInicio() {
        return dataInicio;
    }

    public Data getDataFim() {
        return dataFim;
    }

    public Recinto getRecinto() {
        return recinto;
    }

    public List<Entidade> getLstEnt() {
        return lstEnt;
    }

    public List<Convite> getLstConvites() {
        return lstConvites;
    }

    public List<AssociacaoEntidade> getLstAssEnt() {
        return lstAssEnt;
    }

    public void setBilhetica(Bilhetica bilhetica) {
        this.bilhetica = bilhetica;
    }

    public Bilhetica getBilhetica() {
        return bilhetica;
    }

    public void setReferencia() {
        String letra = "F";
        int numero = id;
        referencia = letra + Integer.toString(numero);
        this.referencia = referencia;
    }

    public void setDesignacao(String designacao) {
        this.designacao = designacao;
    }

    public void setEdicao(String edicao) {
        this.edicao = edicao;
    }

    public void setLocalizacao(String localizacao) {
        this.localizacao = localizacao;
    }

    public void setDataInicio(Data dataInicio) {
        this.dataInicio = dataInicio;
    }

    public void setDataFim(Data dataFim) {
        this.dataFim = dataFim;
    }

    public void setRecinto(Recinto recinto) {
        this.recinto = recinto;
    }

    public Programa getProg() {
        return prog;
    }

    public void setProg(Programa prog) {
        this.prog = prog;
    }

    public List<Atuacao> getLstAtuacoes(String ref) {
        RegistoFestival regFest = null;
        ArrayList<Atuacao> resultado = new ArrayList();
        for (Festival fest : regFest.getLstFestivais()) {
            if (fest.getReferencia().equals(ref)) {
                fest.lstAtuacoes = resultado;
            }
        }
        return resultado;
    }

    public void setLstAtuacoes(List<Atuacao> lstAtuacoes) {
        this.lstAtuacoes = lstAtuacoes;
    }

    public Programa novoPrograma(String ref) {
        RegistoFestival regFest = null;
        for (Festival fest : regFest.getLstFestivais()) {
            if (fest.getReferencia().equals(ref)) {
                prog = new Programa();
            }
        }
        return prog;
    }

    public Programa definirProgramaFestival(String ref) {
        RegistoFestival regFest = null;
        for (Festival fest : regFest.getLstFestivais()) {
            if (fest.getReferencia().equals(ref)) {
                prog.valida();
                prog.setPrograma(lstAtuacoes);
            }
        }
        return prog;
    }

    public boolean guardaEntidade(Entidade ent) {
        if (this.validaEntidade(ent)) {
            return this.lstEnt.add(ent);
        }
        return false;
    }

    public void adicionaAssociacao(AssociacaoEntidade assEnt) {
        lstAssEnt.add(assEnt);
    }

    private boolean validaEntidade(Entidade ent) {
        boolean bRet = false;
        if (ent.valida()) {
            // Escrever aqui o código de validação

            //
            bRet = true;
        }
        return bRet;
    }

    public boolean validaAssociacao(AssociacaoEntidade assEnt) {
        boolean bRet = false;
        if (assEnt.validaAssociacaoEntidade()) {
            // Escrever aqui o código de validação
            //
            bRet = true;
        }
        return true;
    }

    public Convite obterConvite(String artista) {
        Convite result = null;
        for (Convite c : lstConvites) {
            if (c.getArtista().equals(artista)) {
                result = c;
            }
        }
        return result;
    }

    public boolean validaConvite(Convite convite) {
        boolean bRet = false;
        if (convite.validaConvite()) {
            bRet = true;
        }
        return true;
    }

    public boolean valida() {
        //
        return true;
    }

    public Convite novoConvite() {
        return new Convite();
    }

    private boolean adicionaConvite(Convite cvt) {
        return lstConvites.add(cvt);
    }

    public boolean registaConvite(Convite cvt) {
        if (this.validaConvite(cvt)) {
            return adicionaConvite(cvt);
        }
        return false;
    }

    public String obterPrograma() {
        StringBuffer sb = new StringBuffer();
        sb.append(prog);
        return sb.toString();
    }

    public TipoBilhete obterTipoBilhete(String tipoB) {
        TipoBilhete result = null;
        for (TipoBilhete tB : bilhetica.getArrTBE()) {
            if (tipoB.equals(tB.getDesignacao())) {
                tB = result;
            }
        }
        return result;
    }
    
    public void addTipoBilhete(TipoBilhete tb) {
        lstTiposBilhete.add(tb);
    }
    
    public int obterDiasFestival () {
        int nrDias = dataInicio.calcularDiferenca(dataFim);
        return nrDias;
    }
    
    public int obterQtdTiposBilhete () {
        int qtdTipos = lstTiposBilhete.size();
        return qtdTipos;
    }
    
    public void criarBilhetica (int nrDias, int qtdTiposBilhete) {
        bilhetica.distribuicao = new int [nrDias] [qtdTiposBilhete];
    }
    
    public ArrayList<TipoBilhete> obterListaTiposBilheteFestival () {
        return lstTiposBilhete;
}
    
//    public int obterDisponibilidade (int qtd){
//        int result = 0;
//        for ()
//    }

    @Override
    public String toString() {
        return "Festival{" + "referencia=" + referencia + ", designacao=" + designacao + ", edicao=" + edicao + ", localizacao=" + localizacao + ", dataInicio=" + dataInicio + ", dataFim=" + dataFim + ", recinto=" + recinto + ", lstEnt=" + lstEnt + ", lstConvites=" + lstConvites + ", lstAssEnt=" + lstAssEnt + ", prog=" + prog + ", lstAtuacoes=" + lstAtuacoes + '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Festival other = (Festival) obj;
        if (!Objects.equals(this.referencia, other.referencia)) {
            return false;
        }
        if (!Objects.equals(this.designacao, other.designacao)) {
            return false;
        }
        if (!Objects.equals(this.edicao, other.edicao)) {
            return false;
        }
        if (!Objects.equals(this.localizacao, other.localizacao)) {
            return false;
        }
        if (!Objects.equals(this.dataInicio, other.dataInicio)) {
            return false;
        }
        if (!Objects.equals(this.dataFim, other.dataFim)) {
            return false;
        }
        if (!Objects.equals(this.recinto, other.recinto)) {
            return false;
        }
        if (!Objects.equals(this.lstEnt, other.lstEnt)) {
            return false;
        }
        if (!Objects.equals(this.lstConvites, other.lstConvites)) {
            return false;
        }
        if (!Objects.equals(this.lstAssEnt, other.lstAssEnt)) {
            return false;
        }
        if (!Objects.equals(this.prog, other.prog)) {
            return false;
        }
        if (!Objects.equals(this.lstAtuacoes, other.lstAtuacoes)) {
            return false;
        }
        return true;
    }

    
}
