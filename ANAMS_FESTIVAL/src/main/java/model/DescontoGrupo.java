/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author joaoferreira
 */
public class DescontoGrupo implements IDescontavel {

    private static DescontoGrupo desconto;
    private int codigo;
    private String sigla;
    private String descricao;
    private float perc;
    private int numMinPessoas;

    private static final int CODIGO = 12;
    private static final String SIGLA = "FAM";
    private static final String DESCRICAO = "Este desconto aplica-se...";
    private static final float PERC = 0.20f;
    private static final int NUMINPESSOAS = 6;

    private DescontoGrupo() {
        codigo = CODIGO;
        sigla = SIGLA;
        descricao = DESCRICAO;
        numMinPessoas = NUMINPESSOAS;
    }

    public int getCodigo() {
        return codigo;
    }

    public static synchronized DescontoGrupo getinstance() {
        if (desconto == null) {
            desconto = new DescontoGrupo();
        }
        return desconto;
    }

    @Override
    public float obterDesconto(int qtd) {
        float desconto = 0;
        if (qtd >= NUMINPESSOAS) {
            desconto = PERC;
        } else if (qtd < NUMINPESSOAS) {
            desconto = 0;
        }
        return desconto;
    }
}
