/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.Objects;
import utils.Data;

/**
 *
 * @author joaoferreira
 */
public class Vendas {

    private int codigo;
    private int qtdBilhetes;
    private TipoBilhete tipoB;
    private int preco;
    private IDescontavel desconto;
    private float custo;
    private String emailComprador;
    private Data dataCompra;
    private Festival festival;

    static int totalCodigo = 0;
    private static int VALOR_POR_OMISSAO = 0;
    private static float CUSTO_POR_OMISSAO = 0.0f;
    private static String EMAIL_POR_OMISSAO = "N/D";

    public Vendas() {
        this.codigo = totalCodigo++;
        this.qtdBilhetes = VALOR_POR_OMISSAO;
        this.tipoB = new TipoBilhete();
        this.preco = VALOR_POR_OMISSAO;
        this.desconto = DescontoFamilia.getinstance();
        this.desconto = DescontoGrupo.getinstance();
        this.custo = CUSTO_POR_OMISSAO;
        this.dataCompra = new Data();
        this.festival = new Festival();
    }

    public Vendas(int codigo, int qtdBilhetes, TipoBilhete tipoB, int preco, IDescontavel desconto, float custo, String emailComprador, Data dataCompra, Festival festival) {
        this.codigo = totalCodigo++;
        this.qtdBilhetes = qtdBilhetes;
        this.tipoB = new TipoBilhete();
        this.preco = preco;
        this.desconto = DescontoFamilia.getinstance();
        this.desconto = DescontoGrupo.getinstance();
        this.custo = custo;
        this.dataCompra = new Data();
        this.festival = new Festival();
    }

    public int getQtdBilhetes() {
        return qtdBilhetes;
    }

    public void setQtdBilhetes(int qtdBilhetes) {
        this.qtdBilhetes = qtdBilhetes;
    }

    public TipoBilhete getTipoB() {
        return tipoB;
    }

    public void setTipoB(TipoBilhete tipoB) {
        this.tipoB = tipoB;
    }

    public int getPreco() {
        return preco;
    }

    public void setPreco(int preco) {
        this.preco = preco;
    }

    public IDescontavel getDesconto() {
        return desconto;
    }

    public void setDesconto(IDescontavel desconto) {
        this.desconto = desconto;
    }

    public float getCusto() {
        return custo;
    }

    public void setCusto(float custo) {
        this.custo = custo;
    }

    public String getEmailComprador() {
        return emailComprador;
    }

    public void setEmailComprador(String emailComprador) {
        this.emailComprador = emailComprador;
    }

    public Data getDataCompra() {
        return dataCompra;
    }

    public void setDataCompra(Data dataCompra) {
        this.dataCompra = dataCompra;
    }

    public Festival getFestival() {
        return festival;
    }

    public void setFestival(Festival festival) {
        this.festival = festival;
    }

    @Override
    public String toString() {
        return "Vendas{" + "codigo=" + codigo + ", qtdBilhetes=" + qtdBilhetes + ", tipoB=" + tipoB + ", desconto=" + desconto + ", custo=" + custo + ", emailComprador=" + emailComprador + ", dataCompra=" + dataCompra + ", festival=" + festival + '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Vendas other = (Vendas) obj;
        if (this.codigo != other.codigo) {
            return false;
        }
        if (this.qtdBilhetes != other.qtdBilhetes) {
            return false;
        }
        if (Float.floatToIntBits(this.custo) != Float.floatToIntBits(other.custo)) {
            return false;
        }
        if (!Objects.equals(this.emailComprador, other.emailComprador)) {
            return false;
        }
        if (!Objects.equals(this.tipoB, other.tipoB)) {
            return false;
        }
        if (!Objects.equals(this.desconto, other.desconto)) {
            return false;
        }
        if (!Objects.equals(this.dataCompra, other.dataCompra)) {
            return false;
        }
        if (!Objects.equals(this.festival, other.festival)) {
            return false;
        }
        return true;
    }
}
