/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.Objects;

/**
 *
 * @author joaoferreira
 */
public class AssociacaoEntidade {

    private Entidade ent;
    private TipoEntidade TipoEnt;

    public AssociacaoEntidade() {
        this.ent = new Entidade();
        this.TipoEnt = new TipoEntidade();
    }

    public AssociacaoEntidade(Entidade entidade, TipoEntidade tipoEnt) {
        this.ent = entidade;
        this.TipoEnt = tipoEnt;
    }

    public Entidade getEnt() {
        return ent;
    }

    public TipoEntidade getTipoEnt() {
        return TipoEnt;
    }

    public void setEnt(Entidade ent) {
        this.ent = ent;
    }

    public void setTipoEnt(TipoEntidade TipoEnt) {
        this.TipoEnt = TipoEnt;
    }
     public boolean validaAssociacaoEntidade() {
        //
        return true;
    }

    @Override
    public String toString() {
        return "AssociacaoEntidade{" + "ent=" + ent + ", TipoEnt=" + TipoEnt + '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AssociacaoEntidade other = (AssociacaoEntidade) obj;
        if (!Objects.equals(this.ent, other.ent)) {
            return false;
        }
        if (!Objects.equals(this.TipoEnt, other.TipoEnt)) {
            return false;
        }
        return true;
    }
}
