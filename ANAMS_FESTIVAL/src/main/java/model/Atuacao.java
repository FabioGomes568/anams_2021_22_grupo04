/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.Objects;
import utils.Data;

/**
 *
 * @author OLAF
 */
public class Atuacao {
    
    private String artista;
    private Data data;
    private String hora;
    private String duracao;
    private Palco palco;
    
    private static final String STRING_DEFAULT = "";
    
    public Atuacao () {
        this.artista = STRING_DEFAULT;
        this.data = new Data();
        this.hora = STRING_DEFAULT;
        this.duracao = STRING_DEFAULT;
        this.palco = new Palco();     
    }
    
    public Atuacao (String artista, Data data, String hora, String duracao, Palco palco) {
        this.artista = artista;
        this.data = data;
        this.hora = hora;
        this.duracao = duracao;
        this.palco = palco;     
    }

    public String getArtista() {
        return artista;
    }

    public void setArtista(String artista) {
        this.artista = artista;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getDuracao() {
        return duracao;
    }

    public void setDuracao(String duracao) {
        this.duracao = duracao;
    }

    public Palco getPalco() {
        return palco;
    }

    public void setPalco(Palco palco) {
        this.palco = palco;
    }

    @Override
    public String toString() {       
    return "Artista: " + artista + " | Data: " + data + " | Hora: " + hora + " | Duracao: " + duracao + " | Palco: " + palco + "\n";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Atuacao other = (Atuacao) obj;
        if (!Objects.equals(this.artista, other.artista)) {
            return false;
        }
        if (!Objects.equals(this.hora, other.hora)) {
            return false;
        }
        if (!Objects.equals(this.duracao, other.duracao)) {
            return false;
        }
        if (!Objects.equals(this.data, other.data)) {
            return false;
        }
        if (!Objects.equals(this.palco, other.palco)) {
            return false;
        }
        return true;
    }

    
}