/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import utils.Data;

/**
 *
 * @author RS
 */
public class Convite {

    private String artista;
    private String email;
    private int telemovel;
    private int contribuinte;
    private Data dataEnvio;
    private Data dataLimiteResposta;
    private Estado estado;
    List<Estado> estadoValues = Arrays.asList(Estado.values());

    //ReferÍncia : https://stackoverflow.com/questions/29465943/get-enum-values-as-list-of-string-in-java-8
    private static final String ESPECIFICACAO_POR_OMISSAO = "n/d";
    private static final int INT_POR_OMISSAO = 000;

    public enum Estado {
        SEM_RESPOSTA, ACEITE, REJEITADO, ANULADO, NAO_DEFINIDO
    }

    private static final Estado ESTADO_POR_OMISSAO = Estado.NAO_DEFINIDO;

    public Convite() {
        this.artista = ESPECIFICACAO_POR_OMISSAO;
        this.email = ESPECIFICACAO_POR_OMISSAO;
        this.contribuinte = INT_POR_OMISSAO;
        this.telemovel = INT_POR_OMISSAO;
        this.dataEnvio = new Data();
        this.dataLimiteResposta = new Data();
        this.estado = ESTADO_POR_OMISSAO;
    }

    public Convite(String artista, String email, Data dataEnvio, Data dataLimiteResposta, Estado estado) {
        this.artista = artista;
        this.email = email;
        this.telemovel = telemovel;
        this.contribuinte=contribuinte;
        this.dataEnvio = new Data();
        this.dataLimiteResposta = new Data();
        this.estado = estado;
    }

    public String getArtista() {
        return artista;
    }

    public String getEmail() {
        return email;
    }

    public int getTelemovel() {
        return telemovel;
    }

    public int getContribuinte() {
        return contribuinte;
    }

    public Data getDataEnvio() {
        return dataEnvio;
    }

    public Data getDataLimiteResposta() {
        return dataLimiteResposta;
    }

    public Estado getEstado() {
        return estado;
    }

    public List<Estado> getEstadoValues() {
        return estadoValues;
    }

    public Estado devolverEstado(String estado) {
        Estado result = null;
        for (Estado e : Estado.values()) {
            if (e.toString().equals(estado)) {
                result = e;
            }
        }
        return result;
    }

    public void setArtista(String artista) {
        this.artista = artista;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setTelemovel(int telemovel) {
        this.telemovel = telemovel;
    }

    public void setContribuinte(int contribuinte) {
        this.contribuinte = contribuinte;
    }

    public void setDataEnvio(Data dataEnvio) {
        this.dataEnvio = dataEnvio;
    }

    public void setDataLimiteResposta(Data dataLimiteResposta) {
        this.dataLimiteResposta = dataLimiteResposta;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public boolean validaConvite() {
        //
        return true;
    }

    @Override
    public String toString() {
        return "Convite{" + "artista=" + artista + ", email=" + email+ ", telemovel=" + telemovel+ ", contribuinte=" + contribuinte + ", dataEnvio=" + dataEnvio + ", dataLimiteResposta=" + dataLimiteResposta + ", estado=" + estado + '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Convite other = (Convite) obj;
        if (!Objects.equals(this.artista, other.artista)) {
            return false;
        }
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        if (!Objects.equals(this.telemovel, other.telemovel)) {
            return false;
        }
        if (!Objects.equals(this.contribuinte, other.contribuinte)) {
            return false;
        }
        
        if (!Objects.equals(this.dataEnvio, other.dataEnvio)) {
            return false;
        }
        if (!Objects.equals(this.dataLimiteResposta, other.dataLimiteResposta)) {
            return false;
        }
        if (this.estado != other.estado) {
            return false;
        }
        return true;
    }
}