/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author joaoferreira
 */
public class Recinto {
    
    private Palco palco;
    private String designacao;
    private List<Palco> lstPalcos;
    private static final String DESIGNACAO_POR_OMICAO = "RECINTO";

    public Recinto() {
        this.designacao = DESIGNACAO_POR_OMICAO;
        this.lstPalcos = new ArrayList<>();
    }

    public Recinto(String designacao) {
        this.designacao = designacao;
        this.lstPalcos = new ArrayList<>();
    }

    public String getDesignacao() {
        return designacao;
    }

    public List<Palco> getLstPalcos() {
        return lstPalcos;
    }

    public void setLstPalcos(List<Palco> lstPalcos) {
        this.lstPalcos = lstPalcos;
    }

    public Palco getPalco(String designacao) {
        Palco result = null;
        for (Palco p : lstPalcos) {
            if (p.getDesignacao().equalsIgnoreCase(designacao)) {
                result = p;
            }
        }
        return result;
    }
    
    public Palco getPalco() {
        return palco;
    }
    

    @Override
    public String toString() {
        return "Recinto{" + "designacao=" + designacao + ", lstPalcos=" + lstPalcos + '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Recinto other = (Recinto) obj;
        if (!Objects.equals(this.designacao, other.designacao)) {
            return false;
        }
        if (!Objects.equals(this.lstPalcos, other.lstPalcos)) {
            return false;
        }
        return true;
    }

}
