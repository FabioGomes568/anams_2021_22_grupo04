/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author OLAF
 */
public class Programa {
    
    private List<Atuacao> programa;
    
    public Programa () {
        
        this.programa = new ArrayList<Atuacao>();
    }
    
    public Programa (ArrayList<Atuacao> listaAtuacoes) {
        
        this.programa = listaAtuacoes;
    }

    public List<Atuacao> getPrograma() {
        return programa;
    }

    public void setPrograma(List<Atuacao> programa) {
        this.programa = programa;
    }
    
    public boolean valida() {
        // Escrever aqui o códgio de validação
        return true;
    }

    
    @Override
    public String toString() {
        return programa + "";
    }
    
}
