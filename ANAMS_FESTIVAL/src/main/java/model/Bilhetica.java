/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author OLAF
 */
public class Bilhetica {
    
    private List<TipoBilhete> arrTBE;
    int [][] distribuicao;
    int [][] bilhetesVendidos;
    float [] precos;
    
    private static final int [][] INT_DEFAULT = new int [0][0];
    private static final float [] FLOAT_DEFAULT = new float [0];

    public Bilhetica () {
        arrTBE = new ArrayList<TipoBilhete>();
        distribuicao = new int [0][0];
        bilhetesVendidos = new int [0][0];
        precos = new float [0];
    }
    
    public Bilhetica(ArrayList<TipoBilhete> arrTBE, int [][] distribuicao, int [][] bilhetesVendidos, float [] precos) {
        this.arrTBE = arrTBE;
        this.distribuicao = distribuicao;
        this.bilhetesVendidos = bilhetesVendidos;
        this.precos = precos;
    }

    public List<TipoBilhete> getArrTBE() {
        return arrTBE;
    }

    public void setArrTBE(List<TipoBilhete> arrTBE) {
        this.arrTBE = arrTBE;
    }

    public int[][] getDistribuicao() {
        return distribuicao;
    }

    public void setDistribuicao(int[][] distribuicao) {
        this.distribuicao = distribuicao;
    }

    public int[][] getBilhetesVendidos() {
        return bilhetesVendidos;
    }

    public void setBilhetesVendidos(int[][] bilhetesVendidos) {
        this.bilhetesVendidos = bilhetesVendidos;
    }

    public float[] getPrecos() {
        return precos;
    }

    public void setPrecos(float[] precos) {
        this.precos = precos;
    }
    
    public void setQtdBilhetes(int i, int j, int qtdB){
        distribuicao [i][j] = qtdB;
    }
    
    public void setPreco(int i, int preco){
        precos [i] = preco;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Bilhetica other = (Bilhetica) obj;
        if (!Objects.equals(this.arrTBE, other.arrTBE)) {
            return false;
        }
        if (!Arrays.deepEquals(this.distribuicao, other.distribuicao)) {
            return false;
        }
        if (!Arrays.deepEquals(this.bilhetesVendidos, other.bilhetesVendidos)) {
            return false;
        }
        if (!Arrays.equals(this.precos, other.precos)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Bilhetica{" + "arrTBE=" + arrTBE + ", distribuicao=" + distribuicao + ", bilhetesVendidos=" + bilhetesVendidos + ", precos=" + precos + '}';
    }
    
}
