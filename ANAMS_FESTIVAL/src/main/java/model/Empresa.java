/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.ArrayList;
import java.util.List;
import utils.Data;

/**
 *
 * @author OLAF
 */
public class Empresa {

    private ArrayList<TipoEntidade> lstTiposEntidade;
    private final List<Entidade> lstEntidades;
    private RegistoFestival registoFest;
    private List<Vendas> arrVendas;
    private List<IDescontavel> arrDescontos;
    private ArrayList<Artista> lstArtistas;
    private ArrayList<TipoBilhete> lstTiposBilhete;
    //private enum lstTiposBilhete {TIPO_A, TIPO_B, TIPO_C, TIPO_D, VIP};

    private String designacao;

    private static final String DESIGNACAO_POR_OMISSAO = "";

    public Empresa() {
        designacao = DESIGNACAO_POR_OMISSAO;
        this.lstTiposEntidade = new ArrayList<>();
        this.lstEntidades = new ArrayList<>();
        this.registoFest = new RegistoFestival();
        this.arrVendas = new ArrayList<Vendas>();
        this.arrDescontos = new ArrayList<IDescontavel>();
        this.lstArtistas = new ArrayList<>();
        adicionarDescontoFamilia();
        adicionarDescontoGrupo();
        this.lstTiposBilhete = new ArrayList<TipoBilhete>();
    }

    public Empresa(String designacao) {

        this.designacao = designacao;
        this.lstTiposEntidade = new ArrayList<>();
        this.lstEntidades = new ArrayList<>();
        this.registoFest = new RegistoFestival();
        this.arrVendas = new ArrayList<Vendas>();
        this.arrDescontos = new ArrayList<IDescontavel>();
        this.lstArtistas = new ArrayList<>();
        adicionarDescontoFamilia();
        adicionarDescontoGrupo();
        this.lstTiposBilhete = new ArrayList<>();
    }

    public RegistoFestival getRegistoFest() {
        return registoFest;
    }

    private boolean adicionaTipoEntidade(TipoEntidade tipo) {
        return lstTiposEntidade.add(tipo);
    }

    public ArrayList<TipoBilhete> getLstTiposBilhete() {
        return lstTiposBilhete;
    }

    public void setLstTiposBilhete(ArrayList<TipoBilhete> lstTiposBilhete) {
        this.lstTiposBilhete = lstTiposBilhete;
    }

    private boolean adicionaEntidade(Entidade ent) {
        return lstEntidades.add(ent);
    }

    public List<TipoEntidade> obterListaTipoEntidades() {
        return this.lstTiposEntidade;
    }

    public List<Entidade> obterListaEntidades() {
        return this.lstEntidades;
    }

    public TipoEntidade novoTipoEntidade() {
        return new TipoEntidade();
    }

    public Entidade novaEntidade() {
        return new Entidade();
    }

    // Validação global
    public boolean validaTipoEntidade(TipoEntidade tipo) {
        boolean bRet = false;
        if (tipo.valida()) {
            // Escrever aqui o código de validação

            //
            bRet = true;
        }
        return bRet;
    }

    public boolean validaEntidade(Entidade ent) {
        boolean bRet = false;
        if (ent.valida()) {
            // Escrever aqui o código de validação

            //
            bRet = true;
        }
        return bRet;
    }

    public boolean registaTipoEntidade(TipoEntidade tipo) {
        if (this.validaTipoEntidade(tipo)) {
            return adicionaTipoEntidade(tipo);
        }
        return false;
    }

    public boolean registaEntidade(Entidade ent) {
        if (this.validaEntidade(ent)) {
            return adicionaEntidade(ent);
        }
        return false;
    }

    public ArrayList<TipoEntidade> obterListaTiposEnt(String listaTipoEnt) {
        return this.lstTiposEntidade;
    }

    public TipoEntidade obterTipoEntById(int id) {
        TipoEntidade tipoEnt = this.lstTiposEntidade.get(id - 1);
        return tipoEnt;
    }
    
    public TipoBilhete obterTipoBilheteEntById(int id) {
        
        TipoBilhete result = null;
        
        for (TipoBilhete tb : lstTiposBilhete) {
            if (tb.getId() == id) {
                result = tb;
            }
        }
        return result;
    }
    
    public Entidade obterEntidade(String ref) {
        Entidade result = null;
        for (Entidade ent : lstEntidades) {
            if (ent.getReferencia().equals(ent)) {
                result = ent;
            }
        }
        return result;
    }

    private void adicionarDescontoFamilia() {
        IDescontavel desc = DescontoFamilia.getinstance();
        arrDescontos.add(desc);
    }

    private void adicionarDescontoGrupo() {
        IDescontavel desc = DescontoGrupo.getinstance();
        arrDescontos.add(desc);
    }

    public ArrayList<Artista> obterLstArtista() {
        return this.lstArtistas;
    }

    public Artista obterArtista(String nome) {
        Artista result = null;
        for (Artista art : lstArtistas) {
            if (art.getNome().equals(art)) {
                result = art;
            }
        }
        return result;
    }

    public List<IDescontavel> obterListaDescontos() {
        return this.arrDescontos;
    }

    public Vendas criarVenda() {
        return new Vendas();
    }

    public boolean addVenda(Vendas venda) {
        return arrVendas.add(venda);
    }

    public IDescontavel obterDesconto(int codigo) {
        Object result = null;
        DescontoFamilia df = null;
        DescontoGrupo dg = null;
        for (Object o : arrDescontos) {
            if (o.equals(df) && df.getCodigo() == codigo) {
                o = df;
                result = o;
            } else if (o.equals(dg) && dg.getCodigo() == codigo) {
                o = dg;
                result = o;
            }
        }
        return (IDescontavel) result;
    }
}
