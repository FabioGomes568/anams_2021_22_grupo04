/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author RS
 */
public class Artista {

    private String nome;
    private String email;
    private int telemovel;
    private int contribuinte;

    private static final String STRING_POR_OMISSAO = "N/A";
    private static final int INT_POR_OMISSAO = 0;

    public Artista() {
        this.nome = STRING_POR_OMISSAO;
        this.email = STRING_POR_OMISSAO;
        this.telemovel = INT_POR_OMISSAO;
        this.contribuinte = INT_POR_OMISSAO;
    }

    public Artista(String nome, String email, int telemovel, int contribuinte) {
        this.nome = nome;
        this.email = email;
        this.telemovel = telemovel;
        this.contribuinte = contribuinte;
    }

    public String getNome() {
        return nome;
    }

    public String getEmail() {
        return email;
    }

    public int getTelemovel() {
        return telemovel;
    }

    public int getContribuinte() {
        return contribuinte;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setTelemovel(int telemovel) {
        this.telemovel = telemovel;
    }

    public void setContribuinte(int contribuinte) {
        this.contribuinte = contribuinte;
    }

    @Override
    public String toString() {
        return "Artista{" + "nome=" + nome + ", email=" + email + ", telemovel=" + telemovel + ", contribuinte=" + contribuinte + '}';
    }
}
