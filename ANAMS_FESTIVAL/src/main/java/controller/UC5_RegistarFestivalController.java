/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller;

import model.Empresa;
import model.Festival;
import model.Recinto;
import utils.Data;

/**
 *
 * @author OLAF
 */
public class UC5_RegistarFestivalController {

    private Empresa empresa;
    private Festival fest;
    private Recinto recinto;

    public UC5_RegistarFestivalController(Empresa empresa) {
        this.empresa = empresa;
    }

    public void novoFestival() {
        this.fest = empresa.getRegistoFest().novoFestival();
    }

    public void setReferencia() {
        this.fest.setReferencia();
    }

    public void setDesignacao(String designacao) {
        this.fest.setDesignacao(designacao);
    }

    public void setEdicao(String edicao) {
        this.fest.setEdicao(edicao);
    }

    public void setLocalizacao(String localizacao) {
        this.fest.setLocalizacao(localizacao);
    }

    public void setDataIncio(Data data) {
        this.fest.setDataInicio(data);
    }

    public void setDataFim(Data data) {
        this.fest.setDataFim(data);
    }

    public void setRecinto(Recinto recinto) {
        this.fest.setRecinto(recinto);
    }

    public boolean registaFestival() {
        return this.empresa.getRegistoFest().registaFestival(this.fest);
    }

    public String getFestAsString() {
        return this.fest.toString();
    }

    public Recinto getRecinto() {
        return recinto;
    }
}
