/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller;

import java.util.List;
import model.AssociacaoEntidade;
import model.Empresa;
import model.Entidade;
import model.Festival;
import model.RegistoFestival;
import model.TipoEntidade;

/**
 *
 * @author joaoferreira
 */
public class UC6_AssociarEntidadeFestivalController {

    private final Empresa empresa;
    private RegistoFestival registoFestival;
    private Festival fest;
    private Entidade ent;
    private TipoEntidade tipoEnt;

    public UC6_AssociarEntidadeFestivalController(Empresa empresa) {
        this.empresa = empresa;
        //this.registoFestival = empresa.getRegistoFest();
    }

    public List<Festival> obterListaFestivais() {
        return this.empresa.getRegistoFest().obterListaFestivais();
        //return this.registoFestival.getLstFestivais()
    }

    public List<Entidade> obterListaEntidades() {
        return this.empresa.obterListaEntidades();
    }

    public List<TipoEntidade> obterListaTiposEntidadesEmpresa() {
        return this.empresa.obterListaTipoEntidades();
    }

    public List<TipoEntidade> obterListaTipoEntidadesEntidade() {
        return this.ent.obterListaTipoEnt();
    }

    public Festival obterFestival(String ref) {
        fest = empresa.getRegistoFest().obterFestivalReferencia(ref);
        return fest;
    }

    public Entidade obterEntidade(String ref) {
        ent = empresa.obterEntidade(ref);
        return ent;
    }

    public TipoEntidade obterTipoEntidade(String designacao) {
        tipoEnt = ent.oberTipoEntidadeDesignacao(designacao);
        return tipoEnt;
    }

    private AssociacaoEntidade criarAssociacao(Entidade ent, TipoEntidade tipoEnt) { //este método como não necessita de ser chamado fora da classe pode ser do tipo private.
        return new AssociacaoEntidade(ent, tipoEnt);
    }

    public void registaAssociacao() {
        AssociacaoEntidade ass = criarAssociacao(ent, tipoEnt);
        if (this.fest.validaAssociacao(ass)) {
            this.fest.adicionaAssociacao(ass);
        }
    }

    public String obterDadosFestival() { 
        return this.empresa.getRegistoFest().obterFestivalReferencia(fest.getReferencia()).toString();
    }

    public boolean validaFestival() {
        return this.empresa.getRegistoFest().validaFestival(this.fest);
    }

    public boolean validaEntidade(Entidade ent) { //this entidade??
        return this.empresa.validaEntidade(ent);
    }

    public boolean setEntidade(Entidade ent) {

        return this.fest.guardaEntidade(ent);
    } //

    public boolean setTipoEntidade(TipoEntidade tipoEnt) {
        return this.ent.guardaTipoEntidade(tipoEnt);
    }

    public boolean registaFestival(Festival fest) {
        return this.empresa.getRegistoFest().registaFestival(fest);
    }

    public String getFestivalAsString(Festival fest) {
        return fest.toString();
    }
}
