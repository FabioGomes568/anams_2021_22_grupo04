/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller;

import java.util.ArrayList;
import model.Empresa;
import model.Entidade;
import model.TipoEntidade;

/**
 *
 * @author OLAF
 */
public class UC2_RegistarEntidadeController {

    private final Empresa empresa;
    private Entidade ent;
    private String referencia;
    private String designacao;
    private TipoEntidade tipoEnt;

    public UC2_RegistarEntidadeController(Empresa empresa) {
        this.empresa = empresa;
    }

    public void novaEntidade() {
        this.ent = empresa.novaEntidade();
    }

    public void setReferencia() {
        this.ent.setReferencia();
    }

    public void setDesignacao(String designacao) {
        this.ent.setDesignacao(designacao);
    }

    public void setTipoEnt(TipoEntidade tipoEnt) {
        this.ent.setTipoEnt(tipoEnt);
    }

    public ArrayList<TipoEntidade> obterListaTiposEnt(String listaTipoEnt) {

        return empresa.obterListaTiposEnt(listaTipoEnt);
    }

    public TipoEntidade obterTipoEntById(int id) {

        return this.empresa.obterTipoEntById(id);
    }

    public boolean registaEntidade() {
        return this.empresa.registaEntidade(this.ent);
    }

    public String obterDados() {
        return this.ent.obterDados();
    }
}
