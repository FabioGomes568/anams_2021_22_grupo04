/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller;

import java.util.ArrayList;
import java.util.List;
import model.Atuacao;
import model.Empresa;
import model.Festival;
import model.Programa;
import model.RegistoFestival;
import model.TipoEntidade;

/**
 *
 * @author OLAF
 */
public class UC9_DefinirProgramaFestivalController {
    
    private Empresa empresa;
    private TipoEntidade tipo;
    private RegistoFestival regFestival;
    private Programa prog;
    private Festival fest;
    private List<Atuacao> lstAtuacoes;


    public UC9_DefinirProgramaFestivalController(Empresa empresa) {
        this.empresa = empresa;
    }
    public List<Festival> obterListaFest(String listaFest) {

        return regFestival.obterListaFestivais();
    }
    
    public List<Atuacao> obterListaAtuacao (String ref) {

        return fest.getLstAtuacoes(ref);
    }
    
    public void novoPrograma (String ref) {
        this.prog = fest.novoPrograma(ref);
    }
    
    public Programa definirProgramaFestival (String ref) {
        this.prog = fest.definirProgramaFestival(ref);
        return prog;
    }
    
    public String obterPrograma() {
        
        return this.fest.obterPrograma();
    }
}
