/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller;

import java.util.ArrayList;
import java.util.List;
import model.Empresa;
import model.Festival;
import model.Programa;
import model.RegistoFestival;

/**
 *
 * @author OLAF
 */
public class UC10_ConsultarProgramaFestivalController {
    
    private Empresa empresa;
    private Festival festival;
    private RegistoFestival regFestival;
    private Programa prog;

    public UC10_ConsultarProgramaFestivalController(Empresa empresa) {
        
        this.empresa = empresa;
    }
    
    public List<Festival> obterListaFest(String listaFest) {

        return regFestival.obterListaFestivais();
    }
    
    public String getReferencia() {
        
        return festival.getReferencia();
    }
    
    public Programa obterProgramaFestival (String ref) {

        return this.regFestival.obterProgamaFestival(ref);
    }    
}
