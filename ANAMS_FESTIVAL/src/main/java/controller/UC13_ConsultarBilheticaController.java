/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller;

import java.util.List;
import model.Empresa;
import model.Festival;
import model.RegistoFestival;

/**
 *
 * @author RS
 */
public class UC13_ConsultarBilheticaController {

   private Empresa empresa;
    private Festival festival;
    private RegistoFestival regFestival;

    public UC13_ConsultarBilheticaController(Empresa empresa) {
        
        this.empresa = empresa;
    }

    public List<Festival> obterListaFest(String listaFest) {

        return regFestival.obterListaFestivais();
    }
    
    public String getReferencia() {
        
        return festival.getReferencia();
    }
    
 /*   public String obterBilhetica(String ref){
        
        return empresa.obterBilhetica(ref);
    
    }
    */
}
