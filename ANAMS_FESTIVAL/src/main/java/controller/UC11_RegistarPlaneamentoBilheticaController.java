/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller;

import java.util.ArrayList;
import java.util.List;
import model.Bilhetica;
import model.Empresa;
import model.Festival;
import model.RegistoFestival;
import model.TipoBilhete;

/**
 *
 * @author OLAF
 */
public class UC11_RegistarPlaneamentoBilheticaController {

    private final Empresa empresa;
    private RegistoFestival regFestival;
    private Festival fest;
    private Bilhetica bilhetica;
    
    public UC11_RegistarPlaneamentoBilheticaController(Empresa empresa) {
        this.empresa = empresa;
    }
    
    public List<Festival> obterListaFestivais() {
        return this.empresa.getRegistoFest().obterListaFestivais();
    }

    public Festival obterFestival(String ref) {
        fest = empresa.getRegistoFest().obterFestivalReferencia(ref);
        return fest;
    }
    
    public Bilhetica obterBilhetica(String ref) {
        bilhetica = empresa.getRegistoFest().obterBilheticaReferencia(ref);
        return bilhetica;
    }
   
    public ArrayList<TipoBilhete>obterListaTiposBilheteEmpresa(){
        return this.empresa.getLstTiposBilhete();
    }
    
    public TipoBilhete obterTipoBilheteByID(int id){
        return this.empresa.obterTipoBilheteEntById(id);
    }
    
    public void addTipoBilhete(TipoBilhete tb) {
        fest.addTipoBilhete(tb);
    }
    
    public int obterDiasFestival () {
        return fest.obterDiasFestival();
    }
    
    public int obterQtdTiposBilhete () {
        return fest.obterQtdTiposBilhete();
    }
    
    public void criarBilhetica (int nrDias, int qtdTiposBilhete) {
        fest.criarBilhetica(nrDias, qtdTiposBilhete);
    }

    public ArrayList<TipoBilhete> obterListaTiposBilheteFestival(){
        return this.fest.obterListaTiposBilheteFestival();
    }
    
    public void setQtdBilhetes (int i, int j, int qtdB) {
        bilhetica.setQtdBilhetes(i, j, qtdB);
    }
    
    public void setPreco (int i, int preco) {
        bilhetica.setPreco(i, preco);
    }
    
    //public TipoBilhete obterBilhete(String tipo){
      //  tipoBilhete = fest.obterBilhete(tipo);
        //return tipoBilhete;
        //}
}
