/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller;

import java.util.List;
import model.Empresa;
import model.Festival;
import model.IDescontavel;
import model.RegistoFestival;
import model.TipoBilhete;
import model.Vendas;

/**
 *
 * @author joaoferreira
 */
public class UC12_ComprarBilheteController {
//alterar ordem consoante o Sequence Diagram

    private Empresa empresa;
    private Festival fest;
    private RegistoFestival regFestival;
//    private Bilhetica bilhetica; /está dentro da classe festival
//    private TipoBilhete tipoB;
    private Vendas venda;
//    private Bilhetica bilhetica;

    public UC12_ComprarBilheteController(Empresa empresa) {

        this.empresa = empresa;
    }

    public List<Festival> obterListaFestivais() {
        return this.empresa.getRegistoFest().obterListaFestivais();
    }

    public Festival obterFestival(String ref) {
        fest = empresa.getRegistoFest().obterFestivalReferencia(ref);
        return fest;
    }

//    public List<Festival>obterListaTiposBilhete(){ //vai buscar os tipos de bilhete à classe bilhética
//        return this.festival.getListaTiposBilhete;
//    }
//    public TipoBilhete obterBilhete(String tipoB){
//        tipoBilhete = fest.obterBilhete(tipoB);
//        return tipoBilhete;
//        }
//       public int obterDisponibilidade (int qtd) {
//           return this.fest.obterDisponibilidade(qtd)
//       }
    public Vendas criarVenda() {
        return this.empresa.criarVenda();
    }

    public List<IDescontavel> obterListaDescontos() {
        return this.empresa.obterListaDescontos();
    }

    public void setQuantidade(int qtd) {
        this.venda.setQtdBilhetes(qtd);
    }

    public IDescontavel obterDesconto(int codigo) {
        return this.empresa.obterDesconto(codigo);
    }

    public void setDesconto(IDescontavel desconto) {
        this.venda.setDesconto(desconto);
    }
//    public int obterPrecoBilhete (TipoBilhete tipoB){
//        return bilhetica.getPrecoBilhete(tipoB);
//    }

    public void setPreco(int preco) {
        this.venda.setPreco(preco);
    }

    public float calcularValorApagar() {
        float valor = 0.0f;
        int precoTotal = 0;
        int qtd = venda.getQtdBilhetes();
        float valorDesconto = venda.getDesconto().obterDesconto(qtd);
        precoTotal = qtd * venda.getPreco();
        valor = precoTotal * valorDesconto;
        return valor;
    }

    public void setCusto(float custo) {
        this.venda.setCusto(custo);
    }

    public void setFestival(Festival fest) {
        this.venda.setFestival(fest);
    }

    public void registaVenda() {
        this.empresa.addVenda(this.venda);
    }

    public void setEmail(String email) {
        this.venda.setEmailComprador(email);
    }
}
