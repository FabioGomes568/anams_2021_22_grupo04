/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller;

import java.util.List;
import model.Empresa;
import model.Festival;
import model.RegistoFestival;
import model.Convite;
import model.Convite.Estado;

/**
 *
 * @author joaoferreira
 */
public class UC8_AlterarEstadoConviteController {

    private final Empresa empresa;
    private RegistoFestival registoFestival;
    private Festival fest;
    private Convite convite;

    public UC8_AlterarEstadoConviteController(Empresa empresa) {
        this.empresa = empresa;
    }

    public List<Festival> obterListaFestivais() {
        return this.empresa.getRegistoFest().obterListaFestivais();
    }

    public Festival obterFestival(String ref) {
        fest = empresa.getRegistoFest().obterFestivalReferencia(ref);
        return fest;
    }

    public List<Convite> obterListaConvites() {
        return this.fest.getLstConvites();
    }

    public Convite obterConvite(String artista) {
        convite = fest.obterConvite(artista);
        return convite;
    }

    public String obterDadosConvite() {
        convite = fest.obterConvite(this.convite.getArtista());
        return this.convite.toString();
    }

    public void obterEstadosConvite() {
        convite.getEstadoValues();
    }

    public void setEstadoConvite(Estado estado) {
        convite.setEstado(estado);
    }

    public void getEstadoConvite() {
        convite.getEstado();
    }

    public Estado devolverEstado(String estado) {
        return this.convite.devolverEstado(estado);
    }

    public void registaEstadoConvite() {
        this.fest.validaConvite(convite);
    }

    public String obterDadosFestival() {
        return this.empresa.getRegistoFest().obterFestivalReferencia(fest.getReferencia()).toString();
    }
}
