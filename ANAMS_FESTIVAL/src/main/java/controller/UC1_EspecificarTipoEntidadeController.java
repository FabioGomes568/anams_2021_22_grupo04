/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller;

import model.Empresa;
import model.TipoEntidade;

/**
 *
 * @author OLAF
 */
public class UC1_EspecificarTipoEntidadeController {

    private Empresa empresa;
    private TipoEntidade tipo;

    public UC1_EspecificarTipoEntidadeController(Empresa empresa) {
        this.empresa = empresa;
    }

    public void novoTipoEntidade() {
        this.tipo = empresa.novoTipoEntidade();
    }

    public void setDesignacao(String designacao) {
        this.tipo.setDesignacao(designacao);
    }

    public String getDesignacao() {
        return this.tipo.getDesignacao();
    }

    public boolean registaTipoEntidade() {
        return this.empresa.registaTipoEntidade(this.tipo);
    }

    public String getTipoAsString() {
        return this.tipo.toString();
    }

}
