package controller;

import java.util.ArrayList;
import java.util.List;
import model.Artista;
import model.Convite;
import model.Empresa;
import model.Festival;
import model.RegistoFestival;
import utils.Data;

/**
 *
 * @author RS
 */
public class UC7_ConvidarController {

    private Empresa empresa;
    private RegistoFestival regFestival;
    private Festival fest;
    private Convite cvt;
    private Artista art;

    public UC7_ConvidarController(Empresa empresa) { //novo
        this.empresa = empresa;
    }

    public List<Festival> obterListaFest(String listaFest) { //novo
        return regFestival.obterListaFestivais();
    }
    
    public List<Artista> obterLstArtista(String lstArtista) { //novo
        return empresa.obterLstArtista();
    
    }
    public String getReferencia(){

        return fest.getReferencia();
    }

    public void novoConvite() {
        this.cvt = fest.novoConvite();
    }

    public void setArtista(String artista) {
        this.cvt.setArtista(artista);
    }

    public void setEmail(String email) {
        this.cvt.setEmail(email);
    }

    public void setDataEnvio(Data dataEnvio) {
        this.cvt.setDataEnvio(dataEnvio);
    }

    public void setDataLimiteResposta(Data dataLimiteResposta) {
        this.cvt.setDataLimiteResposta(dataLimiteResposta);

    }

    public Artista obterArtista(String nome) {
        return this.empresa.obterArtista(nome);
    }

    public String getEmail() {
        return this.cvt.getEmail();
    }

    public Data getDataEnvio() {
        return this.cvt.getDataEnvio();
    }

    public Data getDataLimiteResposta() {
        return this.cvt.getDataLimiteResposta();

    }

    public boolean registaConvite() {
        return this.fest.registaConvite(this.cvt);
    }

    public String getConviteAsString() {
        return this.cvt.toString();
    }

    public void setTelemovel(int telemovel) {
    this.cvt.setTelemovel(telemovel);    
    }

    public void setContribuinte(int contribuinte) {
    this.cvt.setContribuinte(contribuinte);
    }

    

}
