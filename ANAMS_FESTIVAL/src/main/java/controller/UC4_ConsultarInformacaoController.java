/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller;

import java.util.ArrayList;
import model.Empresa;
import model.Festival;
import utils.Data;

/**
 *
 * @author joaoferreira
 */
public class UC4_ConsultarInformacaoController {

    private Empresa empresa;
    private Festival festival;

    public UC4_ConsultarInformacaoController(Empresa empresa) {
        this.empresa = empresa;
    }

    public ArrayList<Festival> obterInformacaoFestival(Data data) { //alterei de string para arraylist. confirmar com a professora
        return this.empresa.getRegistoFest().obterInformacaoFestival(data);
    }
}
