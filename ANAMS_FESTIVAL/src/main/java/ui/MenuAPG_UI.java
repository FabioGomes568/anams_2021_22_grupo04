/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ui;

import java.io.IOException;
import model.Empresa;
import utils.Utils;

/**
 *
 * @author OLAF
 */
public class MenuAPG_UI {

    private Empresa empresa;
    private String opcao;

    public MenuAPG_UI(Empresa empresa) {
        this.empresa = empresa;
    }

    public void run() throws IOException {
        do {
            System.out.println("###### MENU #####\n\n");
            System.out.println("1. Especificar Tipo de Entidade");
            System.out.println("2. Registar Entidade Colaboradora");
            System.out.println("3. Consultar Informação de um Festival");
            System.out.println("0. Sair");

            opcao = Utils.readLineFromConsole("Introduza opção: ");

            if (opcao.equals("1")) {
                UC1_EspecificarTipoEntidadeUI ui = new UC1_EspecificarTipoEntidadeUI(empresa);
                ui.run();
            }

            if (opcao.equals("2")) {
                UC2_RegistarEntidadeUI ui = new UC2_RegistarEntidadeUI(empresa);
                ui.run();
            }

            if (opcao.equals("3")) {
                UC4_ConsultarInformacaoUI ui = new UC4_ConsultarInformacaoUI(empresa);
                ui.run();
            }

            // Incluir as restantes opções aqui
        } while (!opcao.equals("0"));
    }
}
