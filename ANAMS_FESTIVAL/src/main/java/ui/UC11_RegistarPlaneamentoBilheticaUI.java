/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ui;

import controller.UC11_RegistarPlaneamentoBilheticaController;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import model.Empresa;
import model.Festival;
import model.TipoBilhete;
import utils.Utils;

/**
 *
 * @author OLAF
 */
public class UC11_RegistarPlaneamentoBilheticaUI {
    
    private Empresa empresa;
    private UC11_RegistarPlaneamentoBilheticaController controller;
    private Festival fest;
    private TipoBilhete tb;

    public UC11_RegistarPlaneamentoBilheticaUI(Empresa empresa) {
        this.empresa = empresa;
        controller = new UC11_RegistarPlaneamentoBilheticaController(empresa);
    }

    public void run() {
        
        Scanner ler = new Scanner(System.in);
        Utils.apresentaLista(controller.obterListaFestivais(), "Lista de Festivais : ");
        System.out.println("Insira a referência do festival pretendido : ");
        String fest = ler.next();
        controller.obterFestival(fest);
        controller.obterBilhetica(fest);
        
        Utils.apresentaLista(controller.obterListaTiposBilheteEmpresa(), "Lista de tipos de bilhete da empresa: ");
        System.out.println("Insira o tipo de bilhete pretendido");
        int id = ler.nextInt();
        tb = controller.obterTipoBilheteByID(id);
        controller.addTipoBilhete(tb);
        int nrDias = controller.obterDiasFestival();
        controller.criarBilhetica(nrDias, controller.obterQtdTiposBilhete());
        ArrayList<TipoBilhete> lstTiposBilhete = controller.obterListaTiposBilheteFestival();
        
        int qtdB;
        int preco;
        for (int i = 0; i < lstTiposBilhete.size(); i++) {
            System.out.println("Insira a quantidade de bilhetes do tipo: " + lstTiposBilhete.get(i).getDesignacao());
            for (int j = 0; j < nrDias; j++) {
                System.out.println("Para o dia:" + j);
                qtdB = ler.nextInt();
                controller.setQtdBilhetes(i, j, qtdB);
            }
            System.out.println("Insira o preço dos bilhetes do tipo: " + lstTiposBilhete.get(i).getDesignacao());
            preco = ler.nextInt();
            controller.setPreco(i, preco);            
        }
        System.out.println("Bilhetica consruida:\n" + controller.obterBilhetica(fest) + "\n");
        Utils.confirma("Confirma");

    }

}
