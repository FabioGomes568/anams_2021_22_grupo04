/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ui;

import controller.UC4_ConsultarInformacaoController;
import java.util.Date;
import java.util.Scanner;
import model.Empresa;
import model.Festival;
import utils.Data;
import utils.Utils;

/**
 *
 * @author joaoferreira
 */
public class UC4_ConsultarInformacaoUI {

    private Empresa empresa;
    private UC4_ConsultarInformacaoController controller;

    public UC4_ConsultarInformacaoUI(Empresa empresa) {
        this.empresa = empresa;
        controller = new UC4_ConsultarInformacaoController(empresa);
    }

    public void run() {
        Data dataIntroduzida = introduzData();
        apresentaFestivais(dataIntroduzida);
    }

    private void apresentaFestivais(Data dataIntroduzida) {
        Festival festivalS = (Festival) Utils.apresentaESeleciona(controller.obterInformacaoFestival(dataIntroduzida), "Selecione um festival:");
        System.out.println("Festival selecionado" + festivalS.toString());
    }

    public Data introduzData() {
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o ano :");
        int ano = ler.nextInt();
        System.out.println("Introduza o mês :");
        int mes = ler.nextInt();
        System.out.println("Introduza o dia :");
        int dia = ler.nextInt();
        return new Data(ano, mes, dia);
    }
}
