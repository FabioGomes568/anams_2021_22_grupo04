/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ui;

import controller.UC1_EspecificarTipoEntidadeController;
import model.Empresa;
import utils.Utils;
import java.util.List;
import model.TipoEntidade;

/**
 *
 * @author OLAF
 */
public class UC1_EspecificarTipoEntidadeUI {

    private Empresa empresa;
    private UC1_EspecificarTipoEntidadeController controller;

    public UC1_EspecificarTipoEntidadeUI(Empresa empresa) {
        this.empresa = empresa;
        controller = new UC1_EspecificarTipoEntidadeController(empresa);
    }

    public void run() {
        System.out.println("\nNovo tipo de entidade:");
        controller.novoTipoEntidade();

        introduzDados();

        apresentaDados();

        if (Utils.confirma("Confirma os dados? (S/N)")) {
            if (controller.registaTipoEntidade()) {
                System.out.println("Tipo de entidade registado com sucesso.");
            } else {
                System.out.println("Tipo de entidade não registado.");
            }
        }
    }

    private void introduzDados() {
        String designacao = Utils.readLineFromConsole("Introduza a designação do tipo de entidade: ");

        controller.setDesignacao(designacao);
    }

    private void apresentaDados() {
        System.out.println("\nTipo de Entidade:\n" + controller.getTipoAsString());
    }

    // private void adicionarTipoEntidadeEmpresa (TipoEntidade tipoEnt) {
    //     this.tipoEnt = em
    // }
}
