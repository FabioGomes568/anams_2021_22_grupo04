/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ui;

import controller.UC2_RegistarEntidadeController;
import java.util.ArrayList;
import model.Empresa;
import model.TipoEntidade;
import utils.Utils;

/**
 *
 * @author OLAF
 */
public class UC2_RegistarEntidadeUI {

    private Empresa empresa;
    private UC2_RegistarEntidadeController controller;

    public UC2_RegistarEntidadeUI(Empresa empresa) {
        this.empresa = empresa;
        controller = new UC2_RegistarEntidadeController(empresa);
    }

    public void run() {

        System.out.println("\nNova Entidade:");
        controller.novaEntidade();

        introduzDados();

        apresentaDados();

        if (Utils.confirma("Confirma os dados da Entidade? (S/N)")) {
            if (controller.registaEntidade()) {
                System.out.println("Entidade registada com sucesso. \n");
            } else {
                System.out.println("Entidade não registada. \n");
            }
        }
    }

    private void introduzDados() {

        String designacao = Utils.readLineFromConsole("Introduza a designação da Entidade: ");
        controller.setDesignacao(designacao);

        controller.setReferencia();

        ArrayList<TipoEntidade> listaTipoEnt = controller.obterListaTiposEnt("");
        int contagem = 1;
        System.out.println("\n");
        for (int i = 0; i < listaTipoEnt.size(); i++) {
            System.out.println(contagem + " - " + listaTipoEnt.get(i).toString());
            contagem++;
        }

        TipoEntidade tipoEnt = new TipoEntidade();
        TipoEntidade tipoEnt1 = new TipoEntidade();
        TipoEntidade tipoEnt2 = new TipoEntidade();

        int id = Utils.IntFromConsole("\nIntroduza o número do Tipo de Entidade: ");
        while (id <= 0 || id > listaTipoEnt.size()) {
            id = Utils.IntFromConsole("Número inválido. Introduza novamente o número do Tipo de Entidade: ");
        }
        if (id > 0 && id <= listaTipoEnt.size()) {
            tipoEnt = controller.obterTipoEntById(id);
            controller.setTipoEnt(tipoEnt);
        }

        String opcao = Utils.readLineFromConsole("\nPara adicionar outro Tipo de Entidade prima 'A'.\nPara sair prima qualquer outra letra.");
        if (opcao.equalsIgnoreCase("a")) {
            int contagem1 = 1;
            System.out.println("\n");
            for (int i = 0; i < listaTipoEnt.size(); i++) {
                System.out.println(contagem1 + " - " + listaTipoEnt.get(i).toString()); //controller.obterListaTiposEnt("");
                contagem1++;
            }
            System.out.println("\nTipos de Entidade adicionados:\n" + tipoEnt.toString2());
            int id1 = Utils.IntFromConsole("\nIntroduza o número do Tipo de Entidade: ");
            while (id <= 0 || id > listaTipoEnt.size() || id1 == id) {
                id1 = Utils.IntFromConsole("Número inválido ou tipo repetido. Introduza novamente o número do Tipo de Entidade: ");
            }
            if ((id1 > 0 && id1 <= listaTipoEnt.size()) && id1 != id) {
                tipoEnt1 = controller.obterTipoEntById(id1);
                controller.setTipoEnt(tipoEnt1);
            }
            String opcao2 = Utils.readLineFromConsole("\nPara adicionar outro Tipo de Entidade prima 'A'.\nPara sair prima qualquer outra letra.");
            if (opcao2.equalsIgnoreCase("a")) {
                System.out.println("\n");
                int contagem2 = 1;
                for (int i = 0; i < listaTipoEnt.size(); i++) {
                    System.out.println(contagem2 + " - " + listaTipoEnt.get(i).toString());
                    contagem2++;
                }
                System.out.println("\nTipos de Entidade adicionados:\n" + tipoEnt.toString2() + "\n" + tipoEnt1.toString2());
                int id2 = Utils.IntFromConsole("\nIntroduza o número do Tipo de Entidade: ");
                while (id <= 0 || id > listaTipoEnt.size() || id2 == id || id2 == id1) {
                    id2 = Utils.IntFromConsole("Número inválido ou tipo repetido. Introduza novamente o número do Tipo de Entidade: ");
                }
                if ((id2 > 0 && id2 <= listaTipoEnt.size()) && id2 != id && id2 != id1) {
                    tipoEnt2 = controller.obterTipoEntById(id2);
                    controller.setTipoEnt(tipoEnt2);
                }
            }
        }
    }

    private void apresentaDados() {
        String dados = controller.obterDados();
        System.out.println("\nEntidade:\n" + dados + "\n");
    }
}
