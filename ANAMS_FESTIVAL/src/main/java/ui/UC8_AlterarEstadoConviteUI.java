/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ui;

import controller.UC8_AlterarEstadoConviteController;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import model.Convite.Estado;
import model.Empresa;
import model.Festival;
import utils.Utils;

/**
 *
 * @author joaoferreira
 */
public class UC8_AlterarEstadoConviteUI {

    private Empresa empresa;
    private UC8_AlterarEstadoConviteController controller;

    public UC8_AlterarEstadoConviteUI(Empresa empresa) {
        this.empresa = empresa;
        controller = new UC8_AlterarEstadoConviteController(empresa);
    }

    public void run() {
        alterarEstadoConvite();
        Utils.confirma("Confirma ? ");
    }

    private void alterarEstadoConvite() {
        Scanner ler = new Scanner(System.in);
        Utils.apresentaLista(controller.obterListaFestivais(), "Lista de Festivais : ");
        System.out.println("Insira a referência do festival pretendido : ");
        String refFest = ler.next();
        controller.obterFestival(refFest);
        Utils.apresentaLista(controller.obterListaConvites(), "Lista de Convites : ");
        System.out.println("Insira o nome do artista, o qual pretende alterar o estado do convite : ");
        String artista = ler.next();
        controller.obterConvite(artista);
        controller.obterEstadosConvite();
        System.out.println("Insira um estado : \nTecla 0 - Sem resposta.\n" + "\nTecla 1 - Aceite.\n" + "\nTecla 2 - Rejeitado.\n" + "\nTecla 3 - Anulado.\n");
        int estado = ler.nextInt();
        if (estado > -1 && estado < 4) {
            switch (estado) {
                case 0:
                    String estadoS0 = "SEM_RESPOSTA";
                    Estado e = controller.devolverEstado(estadoS0);
                    controller.setEstadoConvite(e);
                    break;
                case 1:
                    String estadoS1 = "ACEITE";
                    Estado e1 = controller.devolverEstado(estadoS1);
                    controller.setEstadoConvite(e1);
                    break;
                case 2:
                    String estadoS2 = "REJEITADO";
                    Estado e2 = controller.devolverEstado(estadoS2);
                    controller.setEstadoConvite(e2);
                    break;
                case 3:
                    String estadoS3 = "ANULADO";
                    Estado e3 = controller.devolverEstado(estadoS3);
                    controller.setEstadoConvite(e3);
                default:
                    System.out.println("Opção inválida.");
            }
            controller.registaEstadoConvite();
            controller.obterDadosConvite();
            System.out.println("Dados do Festival : " + controller.obterDadosFestival());
        }
    }
}
