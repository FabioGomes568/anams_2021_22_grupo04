/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ui;

import controller.UC12_ComprarBilheteController;
import java.util.Scanner;
import model.Empresa;
import model.Festival;
import model.RegistoFestival;
import utils.Utils;

/**
 *
 * @author joaoferreira
 */
public class UC12_ComprarBilheteUI {

    private Empresa empresa;
    private UC12_ComprarBilheteController controller;
    private Festival fest;
    private RegistoFestival regFestival;
//    private Bilhetica bilhetica;
//    private TipoBilhete tipoB;

    public UC12_ComprarBilheteUI(Empresa empresa) {

        this.empresa = empresa;
        controller = new UC12_ComprarBilheteController(empresa);
    }

    public void run() {
        comprarBilhete();
        Utils.confirma("Confirma");
    }

    private void comprarBilhete() {
        Scanner ler = new Scanner(System.in);
        Utils.apresentaLista(controller.obterListaFestivais(), "Lista de Festivais : ");
        System.out.println("Insira a referência do festival pretendido : ");
        String fest = ler.next();
        controller.obterFestival(fest);
        //Utils.apresentaLista(controller.obterListaTiposBilhete(), "Lista de tipos de bilhete do festival seleccionado : ")
        System.out.println("Insira o tipo de bilhete pretendido");
        String tipoB = ler.next();
        //controller.obterBilhete(tipoB); //criar no controller indo buscar à classe bilhética que estará dentro do festival
        System.out.println("Insira a quantidade pretendida");
        int qtd = ler.nextInt();
        //int qtdDisp = controller.obterDisponibilidade(qtd); //criar no controller indo buscar à classe bilhética que estará dentro do festival
        
        
        
        
        int qtdDisp = 0; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        
        
        
        
        if (qtdDisp >= 0) {
            Utils.apresentaLista(controller.obterListaDescontos(), "Lista de descontos");
            System.out.println("Introduza o código de desconto");
            int codigo = ler.nextInt();
            controller.criarVenda();
            controller.setQuantidade(qtd);
            controller.setDesconto(controller.obterDesconto(codigo));
//            controller.setPreco(controller.obterPrecoBilhete(tipoB));
            controller.setCusto(controller.calcularValorApagar());
            controller.setFestival(controller.obterFestival(fest));
            controller.registaVenda();
            System.out.println(controller.calcularValorApagar());
        } else if (qtdDisp <= 0) {
            System.out.println("Quantidade indisponível. Por favor, seleccione outro tipo de bilhete");
        }

        if (Utils.confirma("Confirma?") == true) {
            System.out.println("Introduz o seu email");
            String email = ler.next();
            controller.setEmail(email);
            Utils.confirma("Confirma o seu endereço de email");
        } else {
            System.out.println("Reveja a operação");
        }
    }
}
