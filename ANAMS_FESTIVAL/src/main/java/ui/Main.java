/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
/**
 *
 * @author OLAF
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import model.Empresa;
import model.TipoEntidade;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt> e Dulce Mota <mdm@isep.ipp.pt>
 * Classe de arranque da aplicação
 */
public class Main {

    public static void main(String[] args) {
        try {   // Construção da empresa
            Empresa empresa = new Empresa("GestFest");

            MenuInicial_UI uiMenu = new MenuInicial_UI(empresa);

            uiMenu.run();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
