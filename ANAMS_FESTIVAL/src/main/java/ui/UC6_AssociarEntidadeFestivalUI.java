/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ui;

import controller.UC6_AssociarEntidadeFestivalController;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import model.AssociacaoEntidade;
import model.Empresa;
import model.Entidade;
import model.Festival;
import model.TipoEntidade;
import utils.Utils;

/**
 *
 * @author joaoferreira
 */
public class UC6_AssociarEntidadeFestivalUI {

    private Empresa empresa;
    private UC6_AssociarEntidadeFestivalController controller;

    public UC6_AssociarEntidadeFestivalUI(Empresa empresa) {
        this.empresa = empresa;
        controller = new UC6_AssociarEntidadeFestivalController(empresa);
    }

    public void run() {
        associaEntidade();

        Utils.confirma("Confirma os dados ? ");
    }

    private void apresentaDados(Festival fest) {
        System.out.println("\nServiço:\n" + controller.getFestivalAsString(fest));
    }

    private void associaEntidade() {
        Scanner ler = new Scanner(System.in);
        Utils.apresentaLista(controller.obterListaFestivais(), "Lista de Festivais : ");
        System.out.println("Insira a referência do festival pretendido : ");
        String refFest = ler.next();
        controller.obterFestival(refFest);
        Utils.apresentaLista(controller.obterListaEntidades(), "Lista de Entidades");
        System.out.println("Insira a referência da entidade pretendida : ");
        String refEnt = ler.next(); 
        controller.obterEntidade(refEnt);  //procura entidade e guarda (não é adicionar, mas sinalizar) na empresa!!
        Utils.apresentaLista(controller.obterListaTipoEntidadesEntidade(), "Lista de tipos de entidades : "); //não é este método
        if (controller.obterListaTiposEntidadesEmpresa() != null) {
            boolean cont = false;
            do {
                System.out.println("Insira a designação do tipo de entidade  : ");
                String designacao = ler.next();
                controller.obterTipoEntidade(designacao);
                controller.registaAssociacao();
            } while (cont);
        }
        System.out.println("Dados do Festival : " + controller.obterDadosFestival());
    }

    //    public void run() {
//        Festival festS = (Festival) Utils.apresentaESeleciona(controller.obterListaFestivais(), "Seleccione o Festival pretendido");
//        if (festS != null) {
//            boolean bNewF = false;
//            do {
//                Entidade ent = (Entidade) Utils.apresentaESeleciona(controller.obterListaEntidades(), "Selecione a Entidade:");
//                if (ent != null) {
//                    boolean bNewE = false;
//                    do {
//                        TipoEntidade tipoEnt = (TipoEntidade) Utils.apresentaESeleciona(controller.obterListaTiposEntidades(), "Selecione o tipo de Entidade:");
//                        controller.setTipoEntidade(tipoEnt);
//                    } while (bNewE);
//                    controller.setEntidade(ent);
//                }
//            } while (bNewF);
//        } else {
//            System.out.println("Não é possivel associar Entidades porque não existem Entidades definidas.");
//        }
//
//        apresentaDados(festS);
//
//        if (Utils.confirma("Confirma os dados do Festival? (S/N)")) {
//            if (controller.registaFestival(festS)) {
//                System.out.println("Festival registado.");
//            } else {
//                System.out.println("Festival não registado.");
//            }
//        }
//    }
}
