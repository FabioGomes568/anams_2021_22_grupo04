/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ui;

import java.io.IOException;
import model.Empresa;
import utils.Utils;

/**
 *
 * @author OLAF
 */
public class MenuUNR_UI {
    
    private Empresa empresa;
    private String opcao;

    public MenuUNR_UI(Empresa empresa) {
        this.empresa = empresa;
    }

    public void run() throws IOException {
        do {
            System.out.println("###### MENU #####\n\n");
            System.out.println("1. Comprar Bilhete");
            System.out.println("0. Sair");

            opcao = Utils.readLineFromConsole("Introduza opção: ");

            if (opcao.equals("1")) {
                UC12_ComprarBilheteUI ui = new UC12_ComprarBilheteUI(empresa);
                ui.run();
            }

        } while (!opcao.equals("0"));
    }
}
