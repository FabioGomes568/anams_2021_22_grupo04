/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ui;

import controller.UC9_DefinirProgramaFestivalController;
import java.util.ArrayList;
import java.util.List;
import model.Atuacao;
import model.Empresa;
import model.Festival;
import model.Programa;
import utils.Utils;

/**
 *
 * @author OLAF
 */
public class UC9_DefinirProgramaFestivalUI {
    
    private Empresa empresa;
    private UC9_DefinirProgramaFestivalController controller;
    private Programa prog;
    
    public UC9_DefinirProgramaFestivalUI(Empresa empresa) {
        this.empresa = empresa;
        controller = new UC9_DefinirProgramaFestivalController(empresa);
    }

    public void run() {
       
        introduzDados();

        apresentaDados();

        
    }
    private void introduzDados() {
        
        List<Festival> listaFest = (ArrayList) controller.obterListaFest("");
        System.out.println("Lista de Festivais:\n");
        System.out.println(controller.obterListaFest(listaFest.toString()));
        String ref = Utils.readLineFromConsole("\nInsira a referência do Festival a que deseja consultar o programa: ");
        controller.novoPrograma(ref);
        
        List<Atuacao> lstAtuacoes = (ArrayList) controller.obterListaAtuacao(ref);
        System.out.println("Lista de Atuacões:\n");
        System.out.println(controller.obterListaAtuacao(ref));
        String escolha ="";
        do {
            escolha = Utils.readLineFromConsole("\nPrima S/N para adicionar as atuações ao programa.\n");
            if (escolha.equalsIgnoreCase("s")) {
                prog = controller.definirProgramaFestival(ref);
                System.out.println("Programa adicionado com sucesso.");
            }
        }
        while (escolha.equals(""));
    }

    private void apresentaDados() {
        
        System.out.println("Programa:\n" + controller.obterPrograma());
    }
}
