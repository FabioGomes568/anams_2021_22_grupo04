package ui;

import controller.UC7_ConvidarController;
import java.util.ArrayList;
import java.util.List;
import model.Artista;
import model.Empresa;
import model.Festival;
import utils.Data;
import utils.Utils;

/**
 *
 * @author RS
 */
public class UC7_ConvidarUI {

    private Empresa empresa;
    private Festival fest;
    private UC7_ConvidarController controller;
    private Artista art;

    public UC7_ConvidarUI(Empresa empresa) {
        this.empresa = empresa;
        controller = new UC7_ConvidarController(empresa);
    }

    public void run() {
        System.out.println("\nNovo Convite");
        controller.novoConvite();
        List<Festival> listaFest = (ArrayList) controller.obterListaFest("");
        List<Artista> listaArt = (ArrayList) controller.obterListaFest("");
        System.out.println(controller.obterListaFest(listaFest.toString()));
        String fref = Utils.readLineFromConsole("\nInsira a referência do Festival a que deseja convidar artista: ");
        System.out.println(controller.obterLstArtista(listaArt.toString()));
        String nome = Utils.readLineFromConsole("\nInsira o nome do artista que deseja convidar ou outro para convidar um artista que não está na lista:");
        if (nome == "outro") {
            introduzDados();
        } else {
            art = controller.obterArtista(nome);
        }

        apresentaDados();

        if (Utils.confirma("Confirma os dados? (S/N)")) {
            if (controller.registaConvite()) {
                System.out.println("Convite registado com sucesso.");
            } else {
                System.out.println("Convite não registado.");
            }
        }
    }

    private void introduzDados() {

        String artista = Utils.readLineFromConsole("Introduza o nome do Artista/Banda: ");
        controller.setArtista(artista);

        String email = Utils.readLineFromConsole("Introduza o email do Artista/Banda: ");
        controller.setEmail(email);

        int contribuinte = Utils.IntFromConsole("Introduza o contribuinte do Artista/Banda: ");
        controller.setContribuinte(contribuinte);
        int telemovel = Utils.IntFromConsole("Introduza o número de telemóvel do Artista/Banda: ");
        controller.setTelemovel(telemovel);

        int ano = Utils.IntFromConsole("Introduza o ano do convite");
        int mes = Utils.IntFromConsole("Introduza o mês do convite:" + "(Formato numérico)");
        int dia = Utils.IntFromConsole("Introduza a dia do convite");
        Data dataEnvio = new Data(ano, mes, dia);
        controller.setDataEnvio(dataEnvio);

        ano = Utils.IntFromConsole("Introduza o ano do convite");
        mes = Utils.IntFromConsole("Introduza o mês do convite:" + "(Formato numérico)");
        dia = Utils.IntFromConsole("Introduza a dia do convite");
        Data dataLimiteResposta = new Data(ano, mes, dia);
        controller.setDataEnvio(dataEnvio);
    }

    private void apresentaDados() {
        System.out.println("\nFestival:\n" + controller.getConviteAsString());
    }
}