/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ui;

import java.io.IOException;
import model.Empresa;
import utils.Utils;

/**
 *
 * @author OLAF
 */
public class MenuRIF_UI {

    private Empresa empresa;
    private String opcao;

    public MenuRIF_UI(Empresa empresa) {
        this.empresa = empresa;
    }

    public void run() throws IOException {
        do {
            System.out.println("###### MENU #####\n\n");
            System.out.println("1. Registar Festival");
            System.out.println("2. Associar Entidade Colaboradora a um Festival");
            System.out.println("3. Consultar Informação de um Festival");
            System.out.println("4. Consultar Programa de um Festival");
            System.out.println("0. Sair");

            opcao = Utils.readLineFromConsole("Introduza opção: ");

            if (opcao.equals("1")) {
                UC5_RegistarFestivalUI ui = new UC5_RegistarFestivalUI(empresa);
                ui.run();
            }

            if (opcao.equals("2")) {
                UC6_AssociarEntidadeFestivalUI ui = new UC6_AssociarEntidadeFestivalUI(empresa);
                ui.run();
            }

            if (opcao.equals("3")) {
                UC4_ConsultarInformacaoUI ui = new UC4_ConsultarInformacaoUI(empresa);
                ui.run();
            }
            
            if (opcao.equals("4")) {
                UC10_ConsultarProgramaFestivalUI ui = new UC10_ConsultarProgramaFestivalUI(empresa);
                ui.run();
            }

            // Incluir as restantes opções aqui
        } while (!opcao.equals("0"));
    }
}
