/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ui;

import java.io.IOException;
import model.Empresa;
import utils.Utils;

/**
 *
 * @author OLAF
 */
public class MenuRBF_UI {
    
    private Empresa empresa;
    private String opcao;

    public MenuRBF_UI(Empresa empresa) {
        this.empresa = empresa;
    }

    public void run() throws IOException {
        do {
            System.out.println("###### MENU #####\n\n");
            System.out.println("1. Registar Planeamento da Bilhética");
            System.out.println("2. Consultar Vendas de bilhetes de um Festival");
            System.out.println("0. Sair");

            opcao = Utils.readLineFromConsole("Introduza opção: ");

            if (opcao.equals("1")) {
                UC11_RegistarPlaneamentoBilheticaUI ui = new UC11_RegistarPlaneamentoBilheticaUI(empresa);
                ui.run();
            }

            if (opcao.equals("2")) {
            //    UC13_ConsultarBilheticaUI ui = new UC13_ConsultarBilheticaUI(empresa);
            //   ui.run();
            }

        } while (!opcao.equals("0"));
    }
}
