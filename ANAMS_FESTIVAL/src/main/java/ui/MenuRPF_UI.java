/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ui;

import java.io.IOException;
import model.Empresa;
import utils.Utils;

/**
 *
 * @author OLAF
 */
public class MenuRPF_UI {

    private Empresa empresa;
    private String opcao;

    public MenuRPF_UI(Empresa empresa) {
        this.empresa = empresa;
    }

    public void run() throws IOException {
        do {
            System.out.println("###### MENU #####\n\n");
            System.out.println("1. Convidar Artista ou Banda");
            System.out.println("2. Alterar Estado de um Convite");
            System.out.println("3. Definir Programa de um Festival");
            System.out.println("4. Consultar Programa de um Festival");
            System.out.println("0. Sair");

            opcao = Utils.readLineFromConsole("Introduza opção: ");

            if (opcao.equals("1")) {
                UC7_ConvidarUI ui = new UC7_ConvidarUI(empresa);
                ui.run();
            }

            if (opcao.equals("2")) {
                UC8_AlterarEstadoConviteUI ui = new UC8_AlterarEstadoConviteUI(empresa);
                ui.run();
            }

            if (opcao.equals("3")) {
                UC9_DefinirProgramaFestivalUI ui = new UC9_DefinirProgramaFestivalUI(empresa);
                ui.run();
            }
            
            if (opcao.equals("4")) {
                UC10_ConsultarProgramaFestivalUI ui = new UC10_ConsultarProgramaFestivalUI(empresa);
                ui.run();
            }

            // Incluir as restantes opções aqui
        } while (!opcao.equals("0"));
    }
}
