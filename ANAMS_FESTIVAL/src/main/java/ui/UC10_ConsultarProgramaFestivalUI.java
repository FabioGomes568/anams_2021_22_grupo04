/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ui;

import controller.UC10_ConsultarProgramaFestivalController;
import java.util.ArrayList;
import java.util.List;
import model.Empresa;
import model.Festival;
import model.Programa;
import model.RegistoFestival;
import utils.Utils;

/**
 *
 * @author OLAF
 */
public class UC10_ConsultarProgramaFestivalUI {
    
    private Empresa empresa;
    private UC10_ConsultarProgramaFestivalController controller;
    private Festival fest;
    private Programa prog;

    public UC10_ConsultarProgramaFestivalUI(Empresa empresa) {
        
        this.empresa = empresa;
        controller = new UC10_ConsultarProgramaFestivalController(empresa);
    }

    public void run() {
        
        introduzDados();

        apresentaDados();

        
    }
    private void introduzDados() {
        
        List<Festival> listaFest = (ArrayList) controller.obterListaFest("");
        System.out.println("Lista de Festivais:\n");
        System.out.println(controller.obterListaFest(listaFest.toString()));
        String ref = Utils.readLineFromConsole("\nInsira a referência do Festival a que deseja consultar o programa: ");
        prog = controller.obterProgramaFestival(ref);
    }

    private void apresentaDados() {
        
        System.out.println(prog.toString());
    }
}
