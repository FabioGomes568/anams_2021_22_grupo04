/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ui;

import controller.UC5_RegistarFestivalController;
import model.Empresa;
import model.Recinto;
import utils.Data;
import utils.Utils;

/**
 *
 * @author OLAF
 */
public class UC5_RegistarFestivalUI {

    private Empresa empresa;
    private UC5_RegistarFestivalController controller;
    private Recinto recinto;

    public UC5_RegistarFestivalUI(Empresa empresa) {
        this.empresa = empresa;
        controller = new UC5_RegistarFestivalController(empresa);
    }

    public void run() {
        System.out.println("\nNovo tipo de Festival:");
        controller.novoFestival();
        introduzDados();
        apresentaDados();
        if (Utils.confirma("Confirma os dados? (S/N)")) {
            if (controller.registaFestival()) {
                System.out.println("Festival registado com sucesso.");
            } else {
                System.out.println("Festival não registado não registado.");
            }
        }
    }

    private void introduzDados() {
        String referencia = null;
        String designacao = null;
        String edicao = null;
        String localizacao = null;
        String designacaoP = null;

        int anoi = 0;
        int mesi = 0;
        int diai = 0;
        int anof = 0;
        int mesf = 0;
        int diaf = 0;
        int qtdPalcos = 0;
        int lotacaoMax = 0;

        while (referencia == null) {
            referencia = Utils.readLineFromConsole("Introduza a Referencia do Festival: ");
            controller.setReferencia();
        }
        while (designacao == null) {
            designacao = Utils.readLineFromConsole("Introduza a designação do Festival: ");
            controller.setDesignacao(designacao);
        }
        while (edicao == null) {
            edicao = Utils.readLineFromConsole("Introduza a edicao do Festival: ");
            controller.setEdicao(edicao);
        }
        while (localizacao == null) {
            localizacao = Utils.readLineFromConsole("Introduza a localizacao do Festival: ");
            controller.setLocalizacao(localizacao);
        }
        while (anoi == 0 || mesi == 0 || diai == 0) {
            anoi = Utils.IntFromConsole("Introduza o ano do inicio do Festival:");
            mesi = Utils.IntFromConsole("Introduza o mês do inicio do Festival:" + "(Formato numérico)");
            diai = Utils.IntFromConsole("Introduza a dia do inicio do Festival:");
            Data dataInicio = new Data(anoi, mesi, diai);
            controller.setDataIncio(dataInicio);
        }
        while (anof == 0 || mesf == 0 || diaf == 0) {
            anof = Utils.IntFromConsole("Introduza o ano do fim do Festival:");
            mesf = Utils.IntFromConsole("Introduza o mês do fim do Festival:" + "(Formato numérico)");
            diaf = Utils.IntFromConsole("Introduza a dia do fim do Festival:");
            Data dataFim = new Data(anof, mesf, diaf);
            controller.setDataFim(dataFim);
        }

        String recinto = Utils.readLineFromConsole("Introduza o recinto  do Festival: ");
        Recinto r = new Recinto(recinto);
        controller.setRecinto(r);

        while (qtdPalcos == 0) { //fix
            qtdPalcos = Utils.IntFromConsole("Introduza a quantidade de palcos do Festival: ");
        }

        for (int i = 0; i<qtdPalcos; i++){ 

        controller.getRecinto().getPalco().setId(i); 
        controller.getRecinto().getPalco().setReferencia();
        while (designacaoP == null) { 

        designacaoP = Utils.readLineFromConsole("Introduza a designacao do palco" + i ); 

        } 

        controller.getRecinto().getPalco().setDesignacao(designacaoP); 

while (lotacaoMax == 0) { 

        lotacaoMax = Utils.IntFromConsole("Introduza a lotacao máxima do palco" + i ); 

        } 

        controller.getRecinto().getPalco().setLotacaoMax(lotacaoMax); 

        }         
    }

    private void apresentaDados() {
        System.out.println("\nFestival:\n" + controller.getFestAsString());
    }
}
